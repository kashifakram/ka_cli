import Vue from 'vue';
import VueRouter from 'vue-router';
import {links} from './routelinks.js';

Vue.use(VueRouter);

const routes = links;

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
});

export default router;