import variables from '../../src/sass_global/_variables.scss';


function toggleBurgerMenu(elem) {
    let overlay = document.getElementById('mobile-overlay');
    let heading = document.getElementById('header_content_wrapper');
    let search = document.getElementById('top-search');

    let bars = elem.dataset.bars;
    
    if(bars === 'true') {
        overlay.style.width = '100%';
        elem.dataset.bars = 'false';
        heading.style.visibility = 'hidden';
        search.style.visibility = 'hidden';
    } else {
        overlay.style.width = '0';
        elem.dataset.bars = 'true';
        heading.style.visibility = 'visible';
        search.style.visibility = 'visible';
    }
    elem.classList.toggle('change');    
}

function openLogin(loginbox, loginbutton, logincaret) {
    if(logincaret.dataset.caret === 'down'){
        logincaret.classList.remove('fa-caret-down');
        logincaret.classList.add('fa-caret-up');
        loginbutton.style.borderBottom = 'none';
        loginbutton.style.borderTop = `1.5px solid ${variables.secColor}`;
        loginbox.style.display = 'block';
        logincaret.dataset.caret = 'up';
    } else {
        logincaret.classList.add('fa-caret-down');
        logincaret.classList.remove('fa-caret-up');
        logincaret.dataset.caret = 'down';
        loginbutton.style.borderBottom = `1.5px solid ${variables.secColor}`;
        loginbutton.style.borderTop = 'none';
        loginbox.style.display = 'none';
    }
    
    return false;
}

export {toggleBurgerMenu, openLogin};