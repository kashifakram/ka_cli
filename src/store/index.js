import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    projects: ['Wenovators', 'LonelyToFamily', 'ButInfact', 'KashifAkram', 'IOSolutions'],
    singleProj: 'Single Proj'
  },
  mutations: {
    changeProject(state, payload){
      state.projects.push(payload);
    },
    changeSingleProj(state, payload){
      state.singleProj = payload;
    }
  },
  actions: {

  },
  modules: {
  },
  getters: {
    projects: state => state.projects,
    projectById: state => id => state.projects[id],
    totalProjs: state => state.projects.length,
  }
});