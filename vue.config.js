module.exports = {
    css: {
        loaderOptions: {
            sass: {
                prependData: `@import "./src/sass_global/_variables.scss";`
            }
        }
    }
};